import { getServerSession } from 'next-auth';
import Login from '../components/Login';
import Logout from '../components/Logout';
import { authOptions } from '@/lib/configs/auth/authOptions';

export default async function Home() {
  const session = await getServerSession(authOptions);
  const roles = session?.user?.roles;

  if (session) {
    return (
      <div>
        <div><Logout /></div>
        {session && <pre>{JSON.stringify(session, null, 2)}</pre>}

        {roles && roles.includes('admin') && (
          <p>Only admin can see this</p>
        )}
      </div>
    );
  }

  return (
    <div>
      <Login />
    </div>
  );
}

