import { AuthOptions } from "next-auth";
import KeycloakProvider from "next-auth/providers/keycloak";
import { jwtDecode } from "jwt-decode";
import { JWTDecodePayload } from "../../../../types/jwtPayload";

export const authOptions: AuthOptions = {
  providers: [
    KeycloakProvider({
      clientId: process.env.KEYCLOAK_CLIENT_ID!,
      clientSecret: process.env.KEYCLOAK_CLIENT_SECRET!,
      issuer: process.env.KEYCLOAK_ISSUER!,
    }),
  ],
  callbacks: {
    async jwt({ token, account }) {
      if (account) {
        const jwt_decode = jwtDecode<JWTDecodePayload>(account.access_token as string);
        const resource = jwt_decode.azp;
        const roles = jwt_decode.resource_access[resource]?.roles;

        return {
          ...token,
          access_token: token.access_token = account.access_token,
          roles: token.roles = roles,
        }
      }
      return token;
    },
    async session({ session, token }) {
      if (token) {
        return {
          ...session,
          user: {
            name: token.name,
            email: token.email,
            image: token.picture,
            roles: token.roles,
          }
        }
      }
      return session;
    },
  },
};

