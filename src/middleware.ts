import { NextAuthMiddlewareOptions, NextRequestWithAuth, withAuth } from 'next-auth/middleware';
import { NextResponse } from 'next/server';

const middleware = (request: NextRequestWithAuth) => {
  const isPrivateRoutes = request.nextUrl.pathname.startsWith('/private');
  const roles = request.nextauth?.token?.roles as string[] | undefined;

  const isAdminUser = roles?.includes('admin');

  if (isPrivateRoutes && !isAdminUser) {
    const url = request.nextUrl.clone();
    url.pathname = '/';
    return NextResponse.redirect(url);
  }
};

const callbackOptions: NextAuthMiddlewareOptions = {};

export default withAuth(middleware, callbackOptions);

export const config = {
  matcher: ['/private']
};

